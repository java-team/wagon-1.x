#!/bin/sh -e

TAR=../wagon_$2.orig.tar.gz
DIR=wagon-$2
# fix 1.0-final -> 1.0
TAG=$(echo wagon-$2 | sed 's/wagon-1.0.0$/wagon-1.0/')

svn export http://svn.apache.org/repos/asf/maven/wagon/tags/$TAG/ $DIR
rm -rf $DIR/lib
tar -c -z -f $TAR $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

