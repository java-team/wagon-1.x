wagon (1.0.0-10) unstable; urgency=medium

  * Team upload.
  * Depend on libplexus-container-default1.5-java instead of
    libplexus-containers-java and libplexus-container-default-java
  * Standards-Version updated to 4.1.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 30 Aug 2017 11:40:46 +0200

wagon (1.0.0-9) unstable; urgency=medium

  * Team upload.
  * Removed the now unused dependency on libjetty-java

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 17 May 2016 15:53:56 +0200

wagon (1.0.0-8) unstable; urgency=medium

  * Team upload.
  * Depend on libcommons-net-java instead of libcommons-net2-java
    (Closes: #800765)
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 3.9.8 (no changes)
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 17 Apr 2016 15:44:37 +0200

wagon (1.0.0-7) unstable; urgency=medium

  * Team upload.
  * Ignore the dependency on wagon-provider-test (Closes: #797264)

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 02 Sep 2015 14:06:48 +0200

wagon (1.0.0-6) unstable; urgency=medium

  * No longer build and install the unused wagon-scm, wagon-provider-test,
    wagon-ssh-common-test and wagon-tck-http modules to reduce the
    dependencies of libwagon-java.
  * Standards-Version updated to 3.9.6 (no changes)
  * Moved the package to Git

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 06 Aug 2015 18:40:07 +0200

wagon (1.0.0-5) unstable; urgency=medium

  * Added a patch to ensure the compatibility with Easymock 3.x
    (Closes: #718872)
  * Disabled the unit tests (LightweightHttpWagonTest hangs randomly)
  * Switch to debhelper level 9
  * debian/control:
    - Standards-Version updated to 3.9.5 (no changes)
    - Don't mention the disabled WebDAV support in the package description
      (Closes: #644926)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 13 Mar 2014 16:47:25 +0100

wagon (1.0.0-4) unstable; urgency=low

  [ Damien Raude-Morvan ]
  * Team upload.
  * Fix FTBFS with latest switch to java7 (Closes: #717280):
    - d/patches/0004-fix-tests-parrallel-run.diff: Provide a different
      port each running test.
    - d/patches/0003-do-not-run-https-tests.patch: Skip HTTPS tests
      for now.

 -- Damien Raude-Morvan <drazzib@debian.org>  Sun, 25 Aug 2013 23:22:18 +0200

wagon (1.0.0-3) unstable; urgency=low

  * debian/watch: Updated to match only 1.x versions
  * Updated Standards-Version to 3.9.4 (no changes)
  * Use canonical URLs for the Vcs-* fields

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 14 May 2013 00:29:48 +0200

wagon (1.0.0-2) unstable; urgency=low

  * Team upload.
  * Drop Michael Koch from Uploaders. (Closes: #654138).
  * Add Build-Depends libjaxen-java to build depends to fix FTBFS, otherwise
    buildd was trying to download it. Thanks to Michael Terry for patch!
    (Closes: #649609).
  * Bump Standards-Version to 3.9.2: no changes needed.
  * d/control: Remove duplicate Homepage fields.

 -- Damien Raude-Morvan <drazzib@debian.org>  Mon, 09 Jan 2012 22:33:51 +0100

wagon (1.0.0-1) unstable; urgency=low

  [ Michael Koch ]
  * Added myself to Uploaders.
  * Build-Depends on debhelper (>= 7).

  [ Torsten Werner ]
  * New upstream release 1.0. We use 1.0.0 to fake a newer version compared to
    1.0-beta-*.
  * Add Build-Depends: libjsoup-java and libcommons-io-java.
  * Change debian/watch to allow download of version 1.0.
  * Switch to source format 3.0.
  * Remove Paul from Uploaders list.
  * Update Standards-Version: 3.9.1.
  * Use Maven to build the package. Switch to Build-Depends: default-jdk-doc.
    (Closes: #567294)
  * Add missing Build-Depends: subversion.

 -- Torsten Werner <twerner@debian.org>  Sat, 03 Sep 2011 11:58:13 +0200

wagon (1.0-beta-6-2) unstable; urgency=low

  * Upload to unstable.

 -- Torsten Werner <twerner@debian.org>  Sun, 09 Aug 2009 23:53:07 +0200

wagon (1.0-beta-6-1) experimental; urgency=low

  * New upstream version
  * Bump up Standards-Version to 3.8.2
  * Remove junit and wagon-provider-test dependency from Maven descriptor
  * Move api documentation to /usr/share/doc/libwagon-java/api

 -- Ludovic Claude <ludovic.claude@laposte.net>  Sat, 25 Jul 2009 18:43:01 +0100

wagon (1.0-beta-5-1) experimental; urgency=low

  * New upstream version (Closes: #531444)
  * Add myself to Uploaders
  * Bump up Standards-Version to 3.8.1
  * Add the Maven POM to the package (Closes: #511870)
  * Add a Build-Depends-Indep dependency on maven-repo-helper and 
    maven-ant-helper, use quilt to patch one of the POM files.
  * Use mh_installpom and mh_installjar to install the POM and the jar to the
    Maven repository
  * Use mh_mavenrepo to generate the update scripts for the Debian parent POM
  * Update watch, orig-tar.sh and the get-orig-sources in debian/rules to use
    SVN tags
  * Remove the patch as it doesn't apply anymore
  * Update the dependency list: remove libcommons-openpgp-java, 
    libcommons-openpgp-java-doc, libganymed-ssh2-java, libjtidy-java, 
    libjtidy-java-doc, libslide-webdavclient-java,
    libplexus-classworlds-java, libplexus-component-api-java
  * Add a dependency on libmaven-scm-java, libnekohtml-java
  * Update debian/build.xml to use build.xml from maven-ant-helper,
    making the build more modular.
  * Renamed wagon.docbase to libwagon-java-doc.docbase to fix documentation

 -- Ludovic Claude <ludovic.claude@laposte.net>  Mon, 06 Jul 2009 23:19:57 +0100

wagon (1.0-beta-2-4) unstable; urgency=low

  * Add missing Depends: ${misc:Depends}.
  * Change Section: java.
  * Fix typo: transferring.
  * Switch to default-jdk. (Closes: #526302)

 -- Torsten Werner <twerner@debian.org>  Wed, 01 Jul 2009 23:27:22 +0200

wagon (1.0-beta-2-3) experimental; urgency=low

  [ Michael Koch ]
  * Added watch file.

  [ Torsten Werner ]
  * Add missing jars.
  * Update control file.
  * Add myself to Uploaders.
  * Add Vcs-* headers.
  * Bump up Standards-Version: 3.8.0.
  * Do no longer quote the full text of the Apache license.

 -- Torsten Werner <twerner@debian.org>  Sat, 29 Nov 2008 12:27:19 +0100

wagon (1.0-beta-2-2) unstable; urgency=low

  * libwagon-java: Fixed missing package
    org.apache.maven.wagon.providers.ssh.knownhost (Closes: #449188)
  * Added new control field "Homepage"

 -- Paul Cager <paul-debian@home.paulcager.org>  Sun, 04 Nov 2007 11:03:16 +0000

wagon (1.0-beta-2-1) unstable; urgency=low

  * Initial release. (Closes: #413518)

 -- Paul Cager <paul-debian@home.paulcager.org>  Mon, 30 Apr 2007 11:40:01 +0100
